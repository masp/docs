<div class="new-page"></div>

## Nutzermodul
### Funktionsumfang
Das Nutzermodul ist für die Verwaltung von Benutzerkonten und für die Überprüfung von Berechtigungen zuständig.

### Berechtigungen prüfen
```go
import (
    "git.mo-mar.de/masp/users"
    "github.com/gin-gonic/gin"
)
func handler(c *gin.Context) {
    // Wenn eine Ressource per ID aufgerufen wird, kann mit HasLinkAccess überprüft werden, ob Gast-Lesezugriff auf die Ressource besteht. Existiert keine ID, sollte `m.GetPrefixByModule(module)` als ID verwendet werden.
	
    id, _ := strconv.Atoi(c.Param("id"))
    if !users.HasLinkAccess(c, id) {
		// RequireUser prüft auf angemeldete Nutzer, RequireAdmin auf Projektadministratoren
        if !users.RequireUser(c) {
            // RequireUser kümmert sich automatisch um alle Fehlermeldungen!
			return
		}
	}
    // ...
    if users.IsAdmin(users.GetUserIDFromContext(c)) {
        // User ist Admin (ohne Fehlermeldung überprüfen)
        // ...
    }
    if users.GetUserIDFromContext(c) > 0 {
        // User ist angemeldet (ohne Fehlermeldung überprüfen)
        // ...
    }
}
```

### Anonyme Links
Mit `users.GenerateLink(nil, id)` kann ein anonymer Link für Lesezugriff generiert werden - per Konvention sollte dieser bei jeder Ressource im `Masp-Permanent-Link` Header mitgesendet werden.

### Go-API
Die Go-API ist in GoDoc dokumentiert und ist unter https://godoc.org/git.mo-mar.de/masp/users zu finden.
