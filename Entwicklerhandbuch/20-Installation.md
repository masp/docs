# Aufsetzen der Entwicklungsumgebung

*Disclaimer:* Wir wollen niemanden aus seiner gewohnten Entwicklungsumgebung holen! Dies ist nur das Setup mit dem wir in der Entwicklung gearbeitet haben, und für welches die Dokumentation und alle Skripte ausgelegt ist.

## Benötigte Software

- **Go** ([golang.org](https://golang.org/dl/)) - Compiler für den Backend-Code
- **Node.js** ([nodejs.org](https://nodejs.org/de/download/current/)) - Compiler für den Frontend-Code
- **gcc** ([gcc.gnu.org](https://gcc.gnu.org/)) (Windows-Version "TDM-GCC" siehe [tdm-gcc.tdragon.net](http://sourceforge.net/projects/tdm-gcc/files/TDM-GCC%20Installer/tdm64-gcc-5.1.0-2.exe/download)) - Compiler für die SQLite-Datenbankbibliothek
- **Git** ([git-scm.com](https://git-scm.com/download)) - Versionskontrollsystem
- **Visual Studio Code** ([code.visualstudio.com](https://code.visualstudio.com/#alt-downloads)) - Integrierte Entwicklungsumgebung, optional
- **GitKraken** ([gitkraken.com](https://www.gitkraken.com/download)) - Grafische Oberfälche für Git, optional

### Installation unter Windows
Nach der Installation aller benötigter Software (siehe oben) müssen noch die Umgebungsvariablen gesetzt werden:

Start, "Umgebungsvariablen für dieses Konto bearbeiten" eintippen & mit Enter bestätigen, oben bei Benutzervariabeln folgendes überprüfen:

- Variable "GOPATH" mit einem leeren Verzeichnis als Wert, in dem Go seine Dependencies speichern kann
- Variable "GOROOT" mit Wert der Go-Installation (enthält Unterordner wie "bin", "src" und "lib")
- Variable "Path" enthält folgende Verzeichnisse ("Bearbeiten")
  - `[GCC-Verzeichnis]/bin`
  - `[Node.js-Verzeichnis]` (Enthält node.exe und node_modules)
  - `[GOROOT]/bin`
  - `[GOPATH]/bin`

Danach sollte der Computer neugestartet werden.

### Installation unter Linux

Debian-basierte Systeme werden direkt unterstützt, bei anderen Systemen ist die Installation ähnlich.

```bash
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb -O /tmp/gitkraken.deb
sudo dpkg -i /tmp/gitkraken
sudo apt-get -f install
rm -f /tmp/gitkraken.deb

wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add - 
echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

sudo apt-get install golang-1.12-go nodejs git gcc codium
```

### Installation unter macOS

Für die Installation wird **Homebrew** ([brew.sh](https://brew.sh/)) empfohlen.

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install go gcc git npm 
brew cask install visual-studio-code gitkraken
```


## VS Code einrichten

Unsere IDE besteht aus Visual Studio Code mit folgenden Plugins erweitert:

- Go (`ms-vscode.go`)
- Vetur (`octref.vetur`)
- EditorConfig for VS Code (`editorconfig.editorconfig`)
- TODO Highlight (`wayou.vscode-todo-highlight`)
- Todo Tree (`gruntfuggly.todo-tree`)
- SQLTools (`mtxr.sqltools`)

Mit den folgenden Befehlen können die Plugins aus der Kommandozeile installiert werden (unter Linux muss mit der obigen Installationsanleitung `codium` statt `code` als Befehl verwendet werden):
```bash
code --install-extension ms-vscode.go \
     --install-extension octref.vetur \
     --install-extension editorconfig.editorconfig \
     --install-extension wayou.vscode-todo-highlight \
     --install-extension gruntfuggly.todo-tree \
     --install-extension mtxr.sqltools
```


## Repository einrichten

Wir brauchen nun einen leeren Ordner, in welchem in einem Terminal folgende Befehle ausgeführt werden:

```bash
go mod init git.mo-mar.de/masp
git clone https://git.mo-mar.de/masp/masp.git
git clone https://git.mo-mar.de/masp/tasks.git
git clone https://git.mo-mar.de/masp/users.git
git clone https://git.mo-mar.de/masp/.vscode.git
```

Dieser Ordner kann jetzt mit VS Code geöffnet werden, der integrierte Debugger kann zum Start des Programms verwendet werden.

