## Aufgabenmodul
### Funktionsumfang
Das Aufgabenmodul ist für die Verwaltung von Aufgaben verantwortlich und liefert dafür im Komplettpaket Code für Frontend, Backend und Datenbank.

### Go-API
Die Go-API ist in GoDoc dokumentiert und ist unter https://godoc.org/git.mo-mar.de/masp/tasks zu finden.
