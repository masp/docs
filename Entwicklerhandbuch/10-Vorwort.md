# Vorwort

***masp*** (make a simple project) ist eine Projektmanagement-Anwendung basierend auf modernen Web-Technologien, die es vor allem kleinen Teams ermöglichen soll, Projekte mit kleinstmöglichem Mehraufwand für die Organisation umzusetzen. Die Software soll genutzt werden, um Projekte zu planen und während des laufenden Projektes einen Überblick zu behalten, wo der aktuelle Projektfortschritt liegt.

Für ein Projekt ist die Software so lange im Einsatz, wie das Projekt in der Planung und Entwicklung benötigt. Eine Software-Instanz soll genau ein Projekt umfassen.

Die Software wurde im Rahmen eines Softwareprojekts unseres Bachelorstudiums an der Otto-von-Guericke-Universität entwickelt. In diesem Zeitraum haben wir das Basis-, Nutzer- und Aufgabenmodul entwickelt.
