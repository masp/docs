---
pagetitle: Entwicklerhandbuch
numbersections: true
lang: DE-de
---

<div class="title">
Entwicklerhandbuch
</div>
<div class="subtitle">
Handbuch für die Weiterentwicklung bestehender und Entwicklung neuer Module
</div>
<div class="author">
Sven Baer
</div>
<div class="author">
Phillipp Engelke
</div>
<div class="author">
Alexa Grube
</div>
<div class="author">
Tim Härtel
</div>
<div class="author">
Moritz Marquardt
</div>
<div class="date">
Otto von Guericke Universität Magdeburg - 4. Juli 2019
</div>
<div class="new-page"></div>

# Inhaltsverzeichnis {-}


<div class="gliederung">
1. Vorwort
2. Aufsetzen einer Entwicklungsumgebung
    1. Windows
    2. Linux
    3. MacOS
    4. Editor einrichten
3. Arbeit mit den Repositories
4. Vorhandene Module
    1. Basismodul
        1. Funktionsumfang
        2. Globale IDs
        3. Go-API
    2. Nutzermodul
        1. Funktionsumfang
        2. Berechtigungen prüfen
        3. Anonyme Links
        4. Go-API
    3. Aufgabenmodul
        1. Funktionsumfang
        2. Go-API
</div>
<div class="new-page"></div>
