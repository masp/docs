# Arbeit mit den Repositories

## Fremde Module hinzufügen oder Module entfernen
Module werden in der Datei `masp/build/modules.txt` verwaltet. Hier kann ein Go-Importpfad pro Zeile definiert werden, der dann als Modul geladen wird. Um die Änderungen an dieser Datei anzuwenden, muss in VS Code mit `Ctrl+Shift+P` die Kommandopalette geöffnet werden und dort `Tasks: Run Task` ausgewählt werden. Im darauffolgenden Fenster gibt es die Option "Clean & Install Dependencies", welche alle in dieser Datei definierten Module installiert und einbindet.


## Erstellen eines neuen Moduls
Um ein neues Modul für masp zu erstellen, muss ein neues Go-Package/-Modul erstellt werden, das vom Go-Compiler gefunden wird.

### Backend-Code
Im Hauptordner des Moduls muss eine `module.go`-Datei liegen, die das Modul beim Basismodul registriert und API-Routen definiert:

<div class="new-page"></div>

`module.go`:
```go
package example

import (
	"path"
	"runtime"

	"git.mo-mar.de/masp/masp"
	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// Module definiert das Modul
type Module struct{}

// M ist die Instanz des Moduls und muss immer so heißen, damit andere Module darauf zugreifen können
var M = Module{}

// m referenziert die API des Basismoduls und ist private, damit kein Modul sich als ein anderes ausgeben kann
var m = masp.RegisterModule(&M)

// Info gibt Informationen über das Modul aus (Name, ID, Version, Dateipfad)
func (*Module) Info() (string, string, *semver.Version, string) {
	_, filename, _, _ := runtime.Caller(0)
	return "Example Module", "example", semver.New("1.0.0"), path.Dir(filename)
}

// Routes richtet die vom Modul benötigten Routen ein
func (*Module) Routes(router *gin.RouterGroup) {
      // Hier GIN-Routen auf den Router definieren.
      // Diese haben die Modul-ID (hier "example") als Pfad-Prefix, z.B. /api/example/helloworld
      // Für mehr Informationen siehe https://godoc.org/github.com/gin-gonic/gin
      router.GET("helloworld", helloWorldHandler)
}

func helloWorldHandler(c *gin.Context) {
    c.String(200, "Hello World\n")
}
```
<div class="new-page"></div>

### Frontend-Code
Für das Frontend muss ein Ordner namens `frontend` angelegt werden. In diesem muss es eine `module.vue` geben, die beispielsweise wie folgt aufgebaut sein kann:

`module.vue`:
```javascript
<script>
export default {
  // Frontend-Routen für den Vue Router (https://router.vuejs.org/) definieren
  // Als Konvention sollten Pfade mit /[module-id] beginnen, einzelne Resourcen sollten unter /[module-id]/[global-id] verfügbar sein.
  routes() {
    return [{ path: "/hello", component: this }];
  },
  // Einstellungen des Moduls, die in den globalen Projekteinstellungen angezeigt werden.
  settings() {
    return {
      module: "example",
      settings: [
        {
            title: "Hallo Welt",
            type: "text",
            name: "hello-world"
        },
        {
            title: "Knopf",
            type: "button",
            name: "anonymous-access"
        },
        {
            title: "Checkbox",
            type: "checkbox",
            name: "unnamed"
        }
      ]
    };
  }
};
</script>
```

Die REST-API kann mit `const res = await api.POST("/api/", { "key": "value" }, { "X-My-Awesome-Header": "totally-awesome" })` angefragt werden, dabei wird ein Fetch-Wrapper ([go.momar.io/fetch-wrapper](https://go.momar.io/fetch-wrapper)) verwendet - in `res.content` steht damit die Antwort des Servers, ansonsten ist `res` eine normale Response ([https://developer.mozilla.org/en-US/docs/Web/API/Response](https://developer.mozilla.org/en-US/docs/Web/API/Response)).

<div class="new-page"></div>

### Datenbank
Die Datenbank kann im Ordner `database` des Moduls verwaltet werden - hier können SQL-Dateien (empfohlenerweise mit dem Dateiformat `000_first_migration.sql`) erstellt werden, die mit **Goose** ([github.com/pressly/goose](https://github.com/pressly/goose) zur Migration verwendet werden. Eine solche Datei könnte beispielsweise wie folgt aussehen:

```sql
-- +goose Up
-- Namen von Datenbanktabellen sollten stets mit der Modul-ID beginnen
CREATE TABLE example_mytable (hello TEXT);
-- +goose Down
DROP TABLE example_mytable;
```

Als Datenbank wird SQLite3 verwendet, die Dokumentation dazu ist auf [https://sqlite.org/lang.html](https://sqlite.org/lang.html) verfügbar.


## Docker-Image erstellen
Um ein funktionstüchtiges Docker-Image zu erstellen, muss im Ordner `masp` der folgende Befehl ausgeführt werden:

```bash
docker build -t example/masp -f build/Dockerfile .
```

Dabei ist zu beachten, dass beim Buildvorgang alle Module erneut heruntergeladen werden und der `master`-Branch verwendet wird.
