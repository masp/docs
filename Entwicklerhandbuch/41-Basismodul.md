# Vorhandene Module
Die Dokumentation der REST-API aller grundlegender Module ist als Swagger-Dokumentation auf [https://get.mo-mar.de/masp/API-Dokumentation.pdf](https://get.mo-mar.de/masp/API-Dokumentation.pdf) verfügbar.

## Basismodul
### Funktionsumfang
Das Basismodul enthält alle grundlegenden Funktionen einer Webanwendung, die masp-Module ausführen kann - dies beinhaltet Logging, Datenbankzugriff, Verwaltung von globalen IDs, Projekteinstellungen, E-Mail-Versand, sowie das grundlegende Design und Layout des Frontends.

### Globale IDs
Eine globale ID ist eindeutig einem Modul zugewiesen, kann aber verwendet werden, um eine Ressource direkt zu referenzieren - beispielsweise kann die Globale ID 212 mit `http://localhost:8080/@212` aufgerufen werden, mithilfe eines anonymen Links (siehe Nutzermodul) auch durch Gäste.

### Go-API
Die Go-API ist in GoDoc dokumentiert und ist unter [https://godoc.org/git.mo-mar.de/masp/masp](https://godoc.org/git.mo-mar.de/masp/masp) zu finden.
