# masp - make a simple project

Software zur Organisation kleiner Projekte anhand von interaktiven Aufgabenlisten.

### ➜ [Benutzerhandbuch](https://get.mo-mar.de/masp/Benutzerhandbuch.pdf) &bull; [Entwicklerhandbuch](https://get.mo-mar.de/masp/Entwicklerhandbuch.pdf) &bull; [API-Dokumentation](https://get.mo-mar.de/masp/API-Dokumentation.pdf) &bull; [Projektbericht](https://get.mo-mar.de/masp/Projektbericht.pdf)

![](https://get.mo-mar.de/screenshots/masp.png)

# Demo

➜ **[demo.projects.mo-mar.de](https://demo.projects.mo-mar.de)**  
**Username:** `demo@example.org`  
**Password:** `demo123`

# Installation zum Ausprobieren
Voraussetzung: [Docker](https://docker.com/)

```bash
docker run -it -v "$PWD/data:/data" -p 8080:8080 -e MASP_FIRST_MAIL=demo@example.org -e MASP_TOKEN=ich-bin-ein-sicherer-token --name masp momar/masp
```

masp ist nun auf http://localhost:8080 erreichbar. Mehr Informationen zur Konfiguration sind im [Benutzerhandbuch](https://get.mo-mar.de/masp/Benutzerhandbuch.pdf) zu finden.
