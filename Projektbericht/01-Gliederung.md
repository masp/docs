---
pagetitle: Projektbericht
numbersections: true
lang: DE-de
---

<div class="title">
Projektbericht
</div>
<div class="subtitle">
Dokumentation unserer projektorganisatorischen Vorgehensweise
</div>
<div class="author">
Sven Baer
</div>
<div class="author">
Phillipp Engelke
</div>
<div class="author">
Alexa Grube
</div>
<div class="author">
Tim Härtel
</div>
<div class="author">
Moritz Marquardt
</div>
<div class="date">
Otto von Guericke Universität Magdeburg - 4. Juli 2019
</div>
<div class="new-page"></div>

# Inhaltsverzeichnis {-}


<div class="gliederung">

1. Vorwort
2. Projektidee & Umsetzung
3. Projekmanagement-Vorgehen
4. Rollen im Team
5. Arbeit mit dem Code
6. Durchführung von Tests
7. Zeitlicher Ablauf
8. Erfahrungen aus dem Softwareprojekt

</div>
<div class="new-page"></div>
