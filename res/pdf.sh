#!/bin/sh
dir="$(dirname "$(readlink -f "$0")")"
tmp_markdown="$(mktemp -u).pdf"
#out="$(echo "$1" | sed 's|\.md$||').pdf"
out="$1"
shift

pandoc -s --css="$dir/page.css" --pdf-engine-opt="--footer-html" --pdf-engine-opt="$dir/footer.html" --pdf-engine-opt=--print-media-type -V papersize=A4 -V margin-top=4cm -V margin-bottom=2cm -V margin-left=2.5cm -V margin-right=2.5cm --from markdown --to html5 -o "$tmp_markdown" "$@"
qpdf --underlay "$dir/background.pdf" --repeat=2 -- "$tmp_markdown" "$out"
