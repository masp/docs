---
pagetitle: Benutzerhandbuch
numbersections: true
lang: DE-de
---

<div class="title">
Benutzerhandbuch
</div>
<div class="subtitle">
Handbuch für die Einrichtung & Verwendung der Software im Alltagsbetrieb
</div>
<div class="author">
Sven Baer
</div>
<div class="author">
Phillipp Engelke
</div>
<div class="author">
Alexa Grube
</div>
<div class="author">
Tim Härtel
</div>
<div class="author">
Moritz Marquardt
</div>
<div class="date">
Otto von Guericke Universität Magdeburg - 4. Juli 2019
</div>
<div class="new-page"></div>

# Inhaltsverzeichnis {-}

<div class="gliederung">
1. Installation
    1. Systemanforderungen
    2. Softwareanforderungen
    3. Installation unter Windows
    4. Installation unter macOS
    5. Installation unter Linux
    6. Umgebungsvariablen im Docker-Container
2. Projekteinstellungen
    1. Allgemein
    2. Einstellungen des Aufgabenmoduls
3. Aufgabenmodul
    1. Übersicht
    2. Aufgaben-Hierarchie
    3. Aufgaben erstellen und bearbeiten
    4. Aufgaben verwalten
4. Nutzermodul
    1. Nutzer anmelden
    2. Benutzerliste
    3. Nutzerrollen
    4. Neuen Nutzer erstellen
    5. Nutzer bearbeiten und löschen
    6. Abmelden
5. Sonstige Informationen
    1. Mobile Ansicht
    2. Feedback, Probleme und Kontakt
</div>

<div class="new-page"></div>
