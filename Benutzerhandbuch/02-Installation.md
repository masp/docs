# Installation

## Systemanforderungen
-	Betriebssystem: Windows Server/Pro, macOS, Linux
-	Prozessor: x86-CPU mit 64-Bit
-	Arbeitsspeicher: mindestens 500 MiB
-	Speicherplatz: 350 MiB verfügbarer Speicher

## Softwareanforderungen
- Docker ab Version 18 ([https://www.docker.com/get-started](https://www.docker.com/get-started))
- Des Weiteren wird empfohlen einen Reverse Proxy zu verwenden, wie beispielsweise Caddy ([https://caddyserver.com/docs/proxy](https://caddyserver.com/docs/proxy))

## Installation unter Windows
1. Stellen Sie sicher, dass Docker gestartet ist
2. Öffnen Sie mit folgender Tastenkombination den Ausführen-Dialog: Windowstaste + R
3. Geben Sie im Fenster `cmd` ein und drücken Sie die Enter-Taste
4. Geben sie folgenden Befehl in das Terminal ein und drücken Sie Enter:
   ```bash
   docker run -d -v "%cd%\data:/data" --name masp -p 8080:8080 -e "MASP_FIRST_MAIL=" -e "MASP_FIRST_PASSWORD=" -e "MASP_TOKEN=" momar/masp
   ```
   Dabei müssen über die Umgebungsvariablen folgende Werte gesetzt werden:
   - `MASP_FIRST_MAIL=test@example.org`, die E-Mail-Adresse des ersten Projektadministrators
   - `MASP_FIRST_PASSWORD=helloworld123`, das Passwort des ersten Projektadministrators
   - `MASP_TOKEN=...`, ein zufälliger anonymer Zugriffstoken (der auf [pag.momar.io](https://pag.momar.io) mit `alphanum/extreme` generiert werden kann)
5. Öffnen Sie in Ihrem Browser folgende Webseite: [http://localhost:8080](http://localhost:8080)
6. Melden Sie sich mit den in Schritt 4 definierten Zugangsdaten an.

## Installation unter macOS
1. Stellen Sie sicher, dass Docker gestartet ist
2. Öffnen Sie mit folgender Tastenkombination die Spotlight-Suche: Command+Space
3. Geben Sie in das Suchfeld folgenden Befehl ein: "Terminal.app"
4. Geben sie folgenden Befehl in das Terminal ein und drücken Sie Enter:
   ```bash
   docker run -d -v "$PWD/data:/data" --name masp -p 8080:8080 -e "MASP_FIRST_MAIL=" -e "MASP_FIRST_PASSWORD=" -e "MASP_TOKEN=" momar/masp
   ```
   Dabei müssen über die Umgebungsvariablen folgende Werte gesetzt werden:
   - `MASP_FIRST_MAIL=test@example.org`, die E-Mail-Adresse des ersten Projektadministrators
   - `MASP_FIRST_PASSWORD=helloworld123`, das Passwort des ersten Projektadministrators
   - `MASP_TOKEN=...`, ein zufälliger anonymer Zugriffstoken (der auf [pag.momar.io](https://pag.momar.io) mit `alphanum/extreme` generiert werden kann)
5. Öffnen Sie in Ihrem Browser folgende Webseite: [http://localhost:8080](http://localhost:8080)
6. Melden Sie sich mit den in Schritt 4 definierten Zugangsdaten an.

## Installation unter Linux
1) Stellen Sie sicher, dass der Docker-Daemon gestartet ist
2) Geben sie folgenden Befehl in das Terminal ein und drücken Sie Enter:
   ```bash
   docker run -d -v "$PWD/data:/data" --name masp -p 8080:8080 -e "MASP_FIRST_MAIL=" -e "MASP_FIRST_PASSWORD=" -e "MASP_TOKEN=" momar/masp
   ```
   Dabei müssen über die Umgebungsvariablen folgende Werte gesetzt werden:
   - `MASP_FIRST_MAIL=test@example.org`, die E-Mail-Adresse des ersten Projektadministrators
   - `MASP_FIRST_PASSWORD=helloworld123`, das Passwort des ersten Projektadministrators
   - `MASP_TOKEN=...`, ein zufälliger anonymer Zugriffstoken (der auf [pag.momar.io](https://pag.momar.io) mit `alphanum/extreme` generiert werden kann)
3) Öffnen Sie in Ihrem Browser folgende Webseite: [http://localhost:8080](http://localhost:8080)
4) Melden Sie sich mit den in Schritt 2 definierten Zugangsdaten an.

<br>

## Umgebungsvariablen im Docker-Container
Mit Umgebungsvariablen kann masp weiter konfiguriert werden. Diese werden dem Docker-Befehl mit `-e "Variable=Wert"` hinzugefügt. Jedoch muss `momar/masp` an letzter Stelle im Befehl stehen.

| Umgebungsvariable   | Beschreibung                                                                                                                                                                                                                                     | Erforderlich |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------ |
| MASP_FIRST_MAIL     | Mit dieser Variable wird die E-Mail-Adresse für den ersten Nutzer festgelegt.                                                                                                                                                                    | Ja           |
| MASP_FIRST_PASSWORD | Mit dieser Variable wird das Passwort für den ersten Nutzer festgelegt. Dieses kann später in der Benutzeroberfläche geändert werden. Wenn kein Passwort gegeben ist, wird ein Passwort generiert und auf der Konsole des Containers ausgegeben. | Nein         |
| MASP_TOKEN          | Diese Variable gibt den Token an, mit dem Links für die Linkfreigabe verschlüsselt werden.                                                                                                                                                       | Ja           |
| MASP_SMTP_HOST      | Gibt den SMTP-Host zum Senden von E-Mails an. Wenn dieser nicht gesetzt ist, läuft die Anwendung ohne E-Mail-Versand - dann wird z. B. das Passwort für einen neuen Nutzer beim Erstellen angezeigt.                                              | Nein         |
| MASP_SMTP_PORT      | Diese Variable setzt den SMTP-Port für den Mailversand.                                                                                                                                                                                          | Nein         |
| MASP_SMTP_USER      | Mit dieser Varibale wird der Nutzer für den SMTP-Host spezifiziert.                                                                                                                                                                              | Nein         |
| MASP_SMTP_PASSWORD  | Diese Variable spezifiziert das Passwort des SMTP-Nutzers.                                                                                                                                                                                       | Nein         |
| MASP_SMTP_SENDER    | Dies Variable setzt den E-Mail-Absender des Systems.                                                                                                                                                                                             | Nein         |
| MASP_LOG            | Setzt das Log-Level des Projekts. Standard ist `Info`. Die Variable kann die Werte `Debug`, `Info`, `Warn` und `Error` annehmen.                                                                                                                 | Nein         |
