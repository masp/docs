#  Sonstige Informationen
## Mobile Ansicht
masp unterstützt eine mobile Ansicht, welche für Smartphones oder Tablets genutzt werden kann. Die Suche, welche sich im Desktop-Modus oben rechts befindet, muss in der mobilen Ansicht ausgeklappt werden.

## Feedback, Probleme und Kontakt
Für Feedback, Anregungen oder bei Problemen oder Fragen wenden Sie sich bitte an die Entwickler über folgende E-Mail-Adresse: [masp@mo-mar.de](mailto:masp@mo-mar.de)
