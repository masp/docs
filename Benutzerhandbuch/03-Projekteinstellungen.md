# Projekteinstellungen

## Allgemein
![](Zahnrad.png){class="l small" style="margin-bottom: 3em"}
Nach der ersten Anmeldung erscheint die Oberfläche des Aufgabenmoduls. Am oberen Bildschirmrand, links neben dem Schriftzug "Neues Projekt", befindet sich ein kleines Zahnrad. Mit einem Klick auf das Zahnrad öffnen sich die Projekteinstellungen.

![](Speichern.png){class="l small" style="margin-bottom: 5em"}
In den Projekteinstellungen, kann der Name des aktuellen Projekts im Feld „Projektname“ verändert werden. Außerdem kann in dem Feld "Beschreibung" eine Projektbeschreibung eingetragen werden. Mit einem Klick auf "Speichern" im oberen, oder im unteren Bildschirmrand werden beide Angaben dauerhaft	gespeichert. Beide Angaben erscheinen in der Kopfzeile von masp und können jederzeit erneut angepasst werden.

## Einstellungen des Aufgabenmoduls

### Links

![](Links.png){class="l" style="margin-bottom: 1em"}
masp bietet die Möglichkeit, Webseiten als Button in der Kopfzeile der Anwendung zu hinterlegen.
Hinterlegte Webseiten können mit einem Namen versehen werden; ein Klick auf einen der weißen Buttons öffnet den hinterlegten Link in einem neuen Browser-Tab.

Ein Name mit Link wird in den Projekteinstellungen hinterlegt (siehe *Projekteinstellungen 2.1*). Unter dem Punkt "Links", kann mithilfe von JSON ein Link mit entsprechenden Namen eingetragen werden. Das Format dafür ist wie folgt:
```json
{ "Linkname 1": "https://link1.example.org", "Linkname 2": "https://link2.example.org" }
```

Die erste Angabe ist der Name, der im Button erscheint; die zweite Angabe die Internetadresse, die aufgerufen werden soll.

### Mögliche Statuswerte
Statuswerte der verschiedenen Aufgaben können in den Projekteinstellungen angepasst werden.

Der Wert ist ein JSON-Array aus Objekten mit folgenden Feldern:

- `name` - Bezeichnung des Status
- `value` - Fortschritt einer Aufgabe mit diesem Status zwischen 0 und 1 (z.B. 50% → 0.5)
- `icon` - verwendetes Symbol von Font Awesome in Form von CSS-Klassen
- `style` - Farbe, Größe, etc. in Form von CSS-Attributen

Standardwert:
```json
[
  { "name": "Offen", "value": 0 },
  { "name": "In Arbeit", "value": 0.3,
    "icon": "fas fa-ellipsis-h",
    "style": { "background-color": "#32A2CF", "color": "white" }
  },
  { "name": "Im Test", "value": 0.7,
    "icon": "fas fa-vial",
    "style": { "background-color": "#FBB829" }
  },
  { "name": "Fertig", "value": 1,
    "icon":"fas fa-check",
    "style": { "background-color": "#59A80F", "color": "white" }
  }
]
```

Für weitere Informationen bezüglich JSON-Objekten kann folgende Seite verwendet werden: [https://wiki.selfhtml.org/wiki/JSON](https://wiki.selfhtml.org/wiki/JSON)
