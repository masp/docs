# Aufgabenmodul

## Übersicht
Im folgenden Beispiel sind die wichtigsten Funktionen von masp erkenntlich gemacht und dient zur ersten Übersicht.

![](Aufgabenmodul.png){class="c"}

## Aufgaben-Hierarchie
Das Aufgabenmodul besteht aus grauen beziehungsweise weißen Blöcken, wobei jeder Block für eine Aufgabe steht. Die Aufgaben sind hierarchisch angeordnet. Die Hierarchie wird durch Einrückung der Aufgaben und durch eine wechselnde Farbgebung verdeutlicht. Innerhalb einer Hierarchieebene sind die Aufgaben listenweise angeordnet. 

Die oberste Ebene der Aufgaben wird immer in weißen Blöcken dargestellt. 
Über das Hinzufügen einer Aufgabe zu einer bestehenden Aufgabe, wird die Hierarchieebene für diese Aufgabe erweitert; eine neue Ebene mit Unteraufgaben entsteht. Die zweite Ebene wird nun in grauen Blöcken dargestellt. Wird eine Aufgabe aus der zweiten Ebene um eine weitere Aufgabe erweitert, entsteht die nächste hierarchische Ebene, die wieder in weiß gekennzeichnet ist, und so weiter. 
Auf jeder Ebene können beliebig viele weitere Aufgaben hinzugefügt werden.

<div class="new-page"></div>

## Aufgaben erstellen und bearbeiten
Aufgaben können einen Titel (Pflichtfeld), eine Beschreibung, Metadaten, Zuweisungen und eine Deadline enthalten.

### Aufgaben hinzufügen
![](neue-aufgabe.png){class="l" style="margin-bottom: 1em"}
Der Button befindet sich sowohl im oberen Teil, als auch im unteren Teil des Bildschirms und fügt am Anfang beziehungsweise am Ende der bestehenden Aufgaben eine weitere hinzu.

![](plus.png){class="l"}
Über dieses Symbol in der oberen rechten Ecke einer Aufgabe fügt man einer Aufgabe eine weitere Unteraufgabe hinzu. Dies kann ebenfalls über die Tastenkombination `Strg+Enter` im Titelfeld erfolgen.

Mit der Tastenkombination `Enter` im Titel-Feld wird eine neue Aufgabe auf der gleichen Ebene direkt unterhalb der aktuellen Aufgabe hinzugefügt.

### Titel
Mit Klick auf den Schriftzug "Titel", kann ein Titel hinzugefügt werden.
Der Titel einer Aufgabe ist ein Pflichtfeld und nicht auf eine Zeichenlänge beschränkt. Sollte, nachdem eine Aufgabe keinerlei weitere Informationen enthält (noch nicht eingetragen oder manuell gelöscht), der Titel ebenfalls entfernt werden, wird der Aufgabenblock automatisch gelöscht, außer es existieren noch Unteraufgaben.

Mithilfe von Ausrufezeichen hinter dem Titel wird der Titel Fett markiert. Zwei Ausrufezeichen verfärben den Titel Orange, drei Ausrufezeichen rot und das vierte Ausrufezeichen löst eine blinkende Animation aus:

![](ausrufezeichen.png){class="f"}

> **Best Practices**: Tragen Sie einen kurzen prägnanten Titel für die Aufgabe ein. Sollte eine Aufgabe Unteraufgaben haben, können Sie auch den Titel eines Aufgabenblocks verwenden, oder auch als Meilenstein.

### Beschreibung
![](beschreibung.png){class="l" style="margin-bottom: 1.5em"}
Beim Klicken auf den Button für Beschreibung wird eine Beschreibung hinzugefügt.
In der Beschreibung können die Aufgaben genauer spezifiert werden und alle relevanten Informationen textbasiert eintragen.

### Metadaten
Metadaten sind Teil der Beschreibung. Um ein Metadatum einzutragen, erstellen Sie eine "Beschreibung" und klicken auf den Button ![](meta.png){height="1em"}. Jetzt auf den Namen bzw. Wert klicken, um die jeweilige Informationen einzutragen. In der mobilen Ansicht muss stattdessen der Button ![](meta-mobil.png){height="1em"} angeklickt werden.

Metadaten dienen dazu, Aufgaben innerhalb eines Projekts zu kategorisieren. Dabei kann eine Kategorie einen Namen und/oder einen Wert haben. Das Metadatum mit dem Namen `weight` und einer entsprechenden Zahl zwischen 1 und 100 als Wert beeinflusst die Fortschrittsanzeige und weist der entsprechenden Aufgabe eine prozentuale Gewichtung zu. Für weitere Information siehe *Fortschrittsanzeige 3.4.1*.

> **Best Practice**: Metadaten sind effektiv, um wiederkehrende Labels oder steckbriefartige Beschreibungen zu verwenden. Beispielsweise können Aufgaben anhand Ihrer Eigenschaften deklariert werden: Ansprechpartner, verwendete Programme, Materiallisten, Orte, Abteilungen, Stundenzahl etc.

### Zuweisung
![](zuweisen.png){class="l"}
Um eine Aufgabe zu einer oder mehreren Personen zuzuweisen, auf das "Personen"-Symbol klicken.

Die Zuweisung dient für Projekte, in denen mehr als eine Person arbeitet. Beim Aktivieren der Zuweisung werden alle möglichen Personen angezeigt und beliebig viele können ausgewählt werden. Zum Eintragen einer neuen Person siehe *Neuen Nutzer erstellen 4.4*. Um eine Person aus der Zuweisung zu entfernen, auf das "x" neben dem jeweiligen Namen klicken.

![](ich.png){class="l"}
Der eigene Benutzername, der in der Zuweisung auftaucht, ist immer violett markiert, während die Namen anderer Teammitglieder grau erscheinen.

<div class="new-page"></div>

### Fristen (Deadlines)
![](wecker.png){class="l"}
Klicken Sie auf das Wecker-Symbol, um eine Frist für eine Aufgabe festzulegen.

Die Frist kann aus einem Kalender ausgewählt werden, wobei auch ein vergangenes Datum verwendet werden kann. Mit dem Uhr-Symbol unter dem Kalender kann eine Zeit eingestellt werden.

Mit dem "Today"-Button wird automatisch das aktuelle Datum ausgewählt. Der "Clear" Button dient dazu die Zeitangabe wieder zu entfernen und mit "Close" schließt sich das Kalender-Menü.

> **Best Practice**: Zu Dokumentationszwecken lassen sich auch Aufgaben mit einem vergangenen Datum eintragen.

### Aufgabe bearbeiten
Aufgaben werden geändert, in dem der jeweilige Inhalt, wie beispielsweise Beschreibung oder Deadline angeklickt und bearbeitet wird.

### Aufgaben löschen
![](trash.png){class="l"}
Neben dem "+" Symbol, um neue Aufgaben anzulegen, befindet sich ein Papierkorbsymbol, welche die entsprechende Aufgabe oder kompletten Aufgabenblock unwiderruflich löscht.

## Aufgaben verwalten
### Fortschrittsanzeige
![](interaktionsleiste.png){class="f"}

Jede Aufgabe mit Unteraufgaben besitzt eine Fortschrittsanzeige unter dem Titel der Aufgabe. Der Fortschritt ist abhängig vom Status der zur Aufgabe gehörenden Unteraufgaben; der Statuswert wirkt sich somit prozentual auf übergeordnete Aufgaben aus. Jeder Statuswechsel beeinflusst den Fortschritt (siehe dazu *Aufgabenstatus 3.4.2*). Erst wenn sowohl die Aufgabe selbst, als auch alle Unteraufgaben einen Wert von 100% haben, beträgt der Fortschritt 100%. Die Fortschrittsanzeige für das gesamte Projekt befindet sich oben neben dem Button für den Linkzugriff.

Sollte eine Aufgabe bzw. Unteraufgabe mehr Gewicht besitzen oder einen größeren Teil des Blocks ausmachen, so kann dies über die Metadaten reflektiert werden. Hierzu fügt man über die Beschreibung das Metadatum mit `weight` als Name hinzu (siehe *Metadaten 3.3.4*) und einen positiven Wert, der das Gewicht reflektiert. Der Standardwert ist 1.

> **Best Practice**: Als Gewichtung für eine Aufgabe bietet sich der geschätzte Aufwand in Arbeitsstunden an.

### Aufgabenstatus
Für jede Aufgabe sind folgende Statuswerte vordefiniert und können verwendet werden:

-	leer: Offen
-	blau: In Arbeit
-	orange: Im Test
-	grün: Fertig (der Aufgabentitel wird durchgestrichen angezeigt)

Um den Status zu wechseln, kann mit der Maus auf das Status-Symbol geklickt werden, damit wird der nächste Status ausgewählt. Das Hovern über dem Status zeigt den aktuellen Status an.
Die vordefinierten Statuswerte können in den Projekteinstellungen (siehe *Mögliche Statuswerte 2.2.2*) verändert, erweitert oder gelöscht werden.

### Aufgabensichten
Unteraufgaben lassen sich mithilfe des "Ausblenden" bzw. "Anzeigen" Button aus und wieder einblenden. Beim Einklappen werden alle zugehörigen Unteraufgaben auf allen tieferen Ebenen ausgeblendet. Die Anzahl der entsprechenden zugehörigen Aufgaben steht in Klammern hinter dem Button zum Ein- bzw. Ausklappen.

![](auge.png){class="l" style="margin-bottom: 1.5em"}
Über das Auge-Symbol lässt sich die aktuelle Sicht auf das Projekt ändern.
Mit Klick auf das Symbol neben einer Aufgabe oder Unteraufgabe wird nur noch diese Aufgabe angezeigt und lässt sich mit allen beschriebenen Funktionen bearbeiten.

> **Best Practice**: Für größere Projekte empfiehlt es sich Aufgaben, an denen man nicht direkt arbeitet einzuklappen und größere Aufgabenblöcke in der Detailansicht zu bearbeiten.

<div class="new-page"></div>

### Drag and Drop
Aufgaben lassen sich per Drag und Drop umsortieren. Jede Aufgabe lässt sich vor oder nach einer Aufgabe platzieren oder auch als Unteraufgabe einer anderen. Dazu wird die Checkbox des Aufgabenstatus einer Aufgabe mit der Maus gezogen und mithilfe des farbig aufleuchteten Zielbereichs platziert.

### Aufgaben exportieren
![](csv.png){class="l"}
Mithilfe des Buttons: "Als CSV exportieren" können alle Aufgaben in eine 	CSV Datei exportiert und heruntergeladen werden.

### Aufgaben für Außenstehende zugänglich machen
![](lesezugriff.png){class="l"}
Für Gäste, Kunden und andere Personen, die nicht als Nutzer eingetragen sind, kann über den Button "Link für Lesezugriff kopieren" ein anonym erreichbarer Link geniert werden. Nach dem Anklicken des Buttons befindet sich der Link in der Zwischenablage und kann mit den Tasten Strg+V in ein Dokument, E-Mail etc. eingefügt werden.
Nicht als Nutzer angemeldete Personen können über diesen Link die Aufgaben mit Unteraufgaben ansehen, jedoch keine Änderungen vornehmen oder zugewiesene Personen oder Deadlines einsehen.

Um nur eine bestimmte Aufgabe oder ein bestimmtes Aufgabenpaket anzeigen zu lassen, wählen Sie über das Augen-Symbol die entsprechende Ansicht (siehe *Aufgabensichten 3.4.3*) und infolgedessen den Button "Link für Lesezugriff kopieren" Button. Dieser Link führt nun zu einer Leseansicht, mit der entsprechenden Aufgabe inklusive eventuellen Unteraufgaben.

### Suche
![](suche.png){class="l" style="margin-bottom: 1em"}
Über die Suche in der oberen rechten Ecke können die Titel der verschiedenen Aufgaben gefiltert werden. Es werden alle Aufgaben Titel gelb hervorgehoben, die einen Teil oder der kompletten Sucheingabe entsprechen.

> **Best Practice**: Eingeklappte Aufgaben werden in der Suche zwar berücksichtigt, aber nicht automatisch aufgeklappt. Über diese Funktion, können die Suchergebnisse eingegrenzt werden.
