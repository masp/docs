# Nutzermodul
Das Nutzermodul ist über den Tab "Nutzer" zu erreichen.

Folgende Nutzerangaben werden gespeichert:

-	Name
-	E-Mail Adresse
-	Nutzer-Rolle (Admin oder normaler Nutzer)
-	Nutzer-ID (wird automatisch vom System vergeben)

Jeder Nutzer erhält ein Profilbild in der oberen rechten Ecke, welches über den Dienst Gravatar bereitgestellt wird. Für nähere Informationen kann folgende Seite besucht werden: [https://de.gravatar.com/](https://de.gravatar.com/)

## Nutzer anmelden
Wenn man nicht angemeldet ist und masp aufruft, wird man automatisch zum Login weitergeleitet. Auf dieser Seite muss man sich mit seiner E-Mail und seinem Passwort authentifizieren. Der Login wird im Browser gespeichert, sodass man sich nicht erneut anmelden muss, wenn man den Browser geschlossen hat. 

## Benutzerliste
Die Benutzerliste zeigt sämtliche Nutzer an. Die angezeigte Tabelle enthält pro Nutzer eine einzigartige ID, den Namen des Nutzers, sowie E-Mail-Adresse und ob der entsprechende Nutzer ein Admin ist. Das Suchfeld in der oberen rechten Ecke kann verwendet werden, um nach dem Namen eines Nutzers zu suchen.

<div class="new-page"></div>

## Nutzerrollen
masp bietet 2 Nutzerrollen an:

**Admin**

Admins können alle Funktionen in masp verwenden. Sie können Aufgaben sowie Nutzer anlegen und verändern.
Für jedes Projekt muss ein Admin vorhanden sein. Sollte nur noch ein Admin in einem Projekt vorhanden sein, so kann dieser nicht seinen Adminstatus entfernen, bis ein anderer Nutzer als Admin eingetragen ist.
Sollte ein Nutzer sein Passwort zu masp vergessen haben, muss ein Admin das Passwort des Nutzers neu setzen. Siehe dazu *Nutzer bearbeiten und löschen 4.5*.

**Nutzer**

Nutzer haben die Möglichkeit, sämtliche Funktionen für die Aufgaben zu verwenden, jedoch können Sie keine Änderungen an anderen Nutzer vornehmen oder neue Nutzer anlegen. Nutzer können jedoch weiterhin ihre eigenen Nutzerangaben ändern und andere Nutzer sehen.

## Neuen Nutzer erstellen
Um einen Nutzer hinzuzufügen, existiert ein Button: "Nutzer hinzufügen" im Nutzer-Modul auf der Seite der Benutzerliste.
Mit Klick auf den Button kann über das entsprechende Feld, Name und E-Mail-Adresse, sowie die Rolle des neuen Nutzers bestimmt werden. Nach dem Bestätigen mit einem Klick auf "Hinzufügen" wird der neue Nutzer dauerhaft gespeichert und erhält an die angegebene E-Mail-Adresse folgende Nachricht:

> Hallo Max Mustermann
>
> Du wurdest einem masp Projekt hinzugefügt! Melde dich unter demo.projects.mo-mar.de an.
>
> Beispiel@test.net \
> 95t,.;pH7jW)
>
> Wir würden Dich bitten dein Passwort nach der Anmeldung zu ändern.
>
> Mit freundlichen Grüßen
> Das MASP Team

Über das angegebene Passwort kann der Nutzer sich anmelden. Es wird empfohlen, das Passwort direkt zu ändern.

Wenn bei der Installation die E-Mail-Funktion nicht mit eingerichtet wurde, dann werden die Daten dem Admin angezeigt und er muss die Zugangsdaten selbst dem Nutzer mitteilen.

## Nutzer bearbeiten und löschen
### Nutzer bearbeiten
![](stift.png){class="l" style="margin-bottom: 8em"}
Über den Stift-Button auf der Benutzerliste lassen sich existierende Nutzer verändern. Der Button öffnet die Angaben des jeweiligen Nutzers, die mit einem Klick in die entsprechenden Felder verändert werden können. Es lassen sich Name, E-Mail Adresse, Passwort und die Rolle des Nutzers ändern. Um das Passwort eines Nutzers zu ändern, muss dieses zweimal eingegeben werden. Sämtliche Änderungen, müssen mit dem eigenen bis dato aktuellem Passwort bestätigen werden, damit die Änderungen dauerhaft übernommen werden. Um Änderungen zu bestätigen, befindet sich ein "Speichern"-Button sowohl am oberen Ende, als auch am unteren Ende der Seite.

Über ein Klick auf den Benutzernamen in der oberen rechten Ecke des Bildschirms, geplant der Nutzer direkt in das Menü, die eigenen Angaben ändern zu können. Auch um seine eigenen Angaben zu ändern, muss das bis dato aktuelle Passwort eingegeben werden.

### Nutzer löschen
![](trash-rot.png){class="l" style="margin-bottom: 3em"}
Um einen Nutzer zu löschen, gibt es zwei Möglichkeiten. 
Die Erste ist, im Nutzer Modul auf der Benutzerliste den Mülleimer-Button zu verwenden, der sich neben dem entsprechenden Nutzer, den man löschen möchte, befindet.
Die zweite Möglichkeit ist, den Nutzer zu bearbeiten und auf den roten "Account Löschen" Button zu klicken. Auf diese Weise können sich Nutzer auch selbst löschen.

## Abmelden
![](logout1.png){class="l"}
Um sich als Nutzer von masp abzumelden, kann man über seinen Nutzernamen bzw. das Nutzerprofilbild in der oberen rechten Ecke die Profileinstellungen öffnen. Hier befindet sich der Abmelden-Button in der oberen rechten Ecke.

![](logout2.png){class="l" style="margin-bottom: 3em"}
Es ist auch möglich, sich von allen angemeldeten Geräten abzumelden. Dazu befindet sich am unteren Ende der Seite der Button: "Abmelden auf allen Geräten". Die Funktion meldet den entsprechenden Nutzer direkt von allen bis dahin benutzten Geräten ab.
